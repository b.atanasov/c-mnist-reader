/*
 * File: mnist_utils.h
 * Author: Boyan Atanasov
 * License: Lincesed under the MIT License (see LICENSE.MD), 2017
 * Project: c_mnist_read
 */

#ifndef C_MNIST_READ_MNIST_UTILS_H
#define C_MNIST_READ_MNIST_UTILS_H

#include <stdint.h>
#include <stdio.h>

/***
 * The function takes a pointer to a 32 bit unsigned integer and
 * reverses it to match the standard of LSB or MSB processors. Works
 * both ways.
 * @param number32 A pointer to an unsigned 32 bit integer.
 * Function modifies the value the parameter points to.
 */
void lsbmsb32(uint32_t * number32);

/***
 * The function takes a pointer to a file stream and determines
 * its length in bytes. Afterwards it resets the pointer to the
 * beginning of the file.
 * @param fileptr A pointer to a file stream.
 * @return A value of type long, corresponding to the byte-length of the file.
 * Returns -1 upon unhandled error
 */
long flen(FILE * fileptr);

#endif //C_MNIST_READ_MNIST_UTILS_H
