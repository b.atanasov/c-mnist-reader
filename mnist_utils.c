/*
 * File: mnist_utils.c
 * Author: Boyan Atanasov
 * License: Lincesed under the MIT License (see LICENSE.MD), 2017
 * Project: c_mnist_read
 */

#include "mnist_utils.h"

void lsbmsb32(uint32_t * number32) {
    uint32_t bytes[4];

    bytes[1] = (*number32 & 0x000000ff) << 24u;
    bytes[2] = (*number32 & 0x0000ff00) << 8u;
    bytes[3] = (*number32 & 0x00ff0000) >> 8u;
    bytes[4] = (*number32 & 0xff000000) >> 24u;

    *number32 = bytes[1] | bytes[2] | bytes[3] | bytes[4];
}

long flen(FILE * fileptr) {
    long filelen = -1;
    /* seek to end of file */
    fseek(fileptr, 0, SEEK_END);
    /* tell length of file, store in "filelen" */
    filelen = ftell(fileptr);
    /* rewind to start of file */
    fseek(fileptr, 0, SEEK_SET);
    return filelen;
}