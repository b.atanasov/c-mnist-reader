c_mnist_read: main.o mnist_file.o mnist_utils.o
	$(CC) main.o mnist_file.o mnist_utils.o -o c_mnist_read

main.o: main.c
	$(CC) -c main.c

mnist_file.o: mnist_file.c mnist_file.h
	$(CC) -c mnist_file.c

mnist_utils.o: mnist_utils.c mnist_utils.h
	$(CC) -c mnist_utils.c

