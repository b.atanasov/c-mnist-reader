/*
 * File: mnist_file.c
 * Author: Boyan Atanasov
 * License: Lincesed under the MIT License (see LICENSE.MD), 2017
 * Project: c_mnist_read
 */

#include "mnist_file.h"
#include "mnist_utils.h"

IDX3_fileinfo * readIDX3info(char * filepath) {
    printf("***\n [mnistimg.h::readIDX3info]\n--- READING NEW FILE HEADER ---\n");
    IDX3_fileinfo * info = (IDX3_fileinfo *) malloc( sizeof(IDX3_fileinfo));
    info->filepath = filepath; // add filepath to info
    FILE * fileptr; // open the file stream
    fileptr = fopen(filepath, "r");
    info->filelen = flen(fileptr); // find the file length in bytes
    fread(&info->magic_number, sizeof(uint32_t), 1, fileptr); // read the magic number from the file header
    lsbmsb32(&info->magic_number); // LSB to MSB
    fread(&info->num_images, sizeof(uint32_t), 1, fileptr); // read the number of items from header
    lsbmsb32(&info->num_images); // LSB to MSB
    /* read the width and height of images from header */
    fread(&info->image_width, sizeof(uint32_t), 1, fileptr);
    fread(&info->image_height, sizeof(uint32_t), 1, fileptr);
    lsbmsb32(&info->image_width);
    lsbmsb32(&info->image_height);
    /* record end of header byte */
    info->byte_content_start = ftell(fileptr);
    /* print file info */
    printf("> The length of \"%s\" is: %li B.\n", info->filepath, info->filelen);
    printf("> The magic number of the file is: 0x%.8x.\n", info->magic_number);
    printf("> The number of images in the file is: %d.\n", info->num_images);
    printf("> The images have a width of %d and a height of %d bytes (pixels).\n", info->image_width, info->image_height);
    printf("> The end of header byte is: %li/%li\n", info->byte_content_start, info->filelen);
    fclose(fileptr);
    return info;
}

IDX1_fileinfo * readIDX1info(char * filepath) {
    IDX1_fileinfo * info = (IDX1_fileinfo *) malloc(sizeof(IDX1_fileinfo));
    printf("***\n [mnistimg.h::readIDX1info]\n--- READING NEW FILE HEADER ---\n");
    info->filepath = filepath; // add file path to info
    FILE * fileptr;
    fileptr = fopen(filepath, "r"); // open the file stream
    info->filelen = flen(fileptr); // find the file length in bytes
    fread(&info->magic_number, sizeof(uint32_t), 1, fileptr); // read the magic number from the header
    lsbmsb32(&info->magic_number); // LSB to MSB
    fread(&info->num_labels, sizeof(uint32_t), 1, fileptr); // read the number of items from the header
    lsbmsb32(&info->num_labels);
    /* record end of header byte */
    info->byte_content_start = ftell(fileptr);
    /* print file info */
    printf("> The length of \"%s\" is: %li B.\n", info->filepath, info->filelen);
    printf("> The magic number of the file is: 0x%.8x.\n", info->magic_number);
    printf("> The number of labels in the file is: %d.\n", info->num_labels);
    printf("> The current byte is: %li/%li\n", info->byte_content_start, info->filelen);
    fclose(fileptr);
    return info;
}

MNIST_img * readIDX3content(IDX3_fileinfo * fileinfo) {
    /* allocate memory for the images array */
    MNIST_img * arr_images = (MNIST_img *) malloc( fileinfo->num_images * sizeof(MNIST_img));
    long offset = fileinfo->byte_content_start;
    /* open file stream */
    FILE * fileptr;
    fileptr = fopen(fileinfo->filepath, "r");
    /* seek by offset and read images */
    fseek(fileptr, offset, SEEK_SET);
    fread(arr_images, sizeof(MNIST_img), fileinfo->num_images, fileptr);
    return arr_images;
}

MNIST_lbl * readIDX1content(IDX1_fileinfo * fileinfo) {
    /* allocate memory for the labels array */
    MNIST_lbl * arr_labels = (MNIST_lbl *) malloc( fileinfo->num_labels * sizeof(MNIST_lbl));
    long offset = fileinfo->byte_content_start;
    /* open file stream */
    FILE * fileptr;
    fileptr = fopen(fileinfo->filepath, "r");
    /* seek by offset and read labels */
    fseek(fileptr, offset, SEEK_SET);
    fread(arr_labels, sizeof(MNIST_lbl), fileinfo->num_labels, fileptr);
    return arr_labels;
}

