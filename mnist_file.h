/*
 * File: mnist_file.h
 * Author: Boyan Atanasov
 * License: Lincesed under the MIT License (see LICENSE.MD), 2017
 * Project: c_mnist_read
 */

#ifndef C_MNIST_READ_MNISTFILE_H
#define C_MNIST_READ_MNISTFILE_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* Most significant byte first magic number for MNIST image files */
static const uint32_t MSB_MAGIC_IMG = 0x00000803;
/* Most significant byte first magic number for MNIST label files */
static const uint32_t MSB_MAGIC_LBL = 0x00000801;
/* IDX3 header size */
static const uint32_t IDX3_HEADER_BYTES = 16;
/* IDX1 header size */
static const uint32_t IDX1_HEADER_BYTES = 8;

typedef struct mnist_image MNIST_img;
struct mnist_image {
    uint8_t img_pixel[28][28];
};

typedef uint8_t MNIST_lbl;

typedef struct mnist_imagefile_header IDX3_fileinfo;
struct mnist_imagefile_header {
    char * filepath;
    long filelen;
    uint32_t magic_number;
    uint32_t num_images;
    uint32_t image_width;
    uint32_t image_height;
    long byte_content_start;
};

typedef struct mnist_labelfile_header IDX1_fileinfo;
struct mnist_labelfile_header {
    char * filepath;
    long filelen;
    uint32_t magic_number;
    uint32_t num_labels;
    long byte_content_start;
};

/***
 * This function takes in the file path, reads the header bytes
 * of the IDX3_ubyte file and returns a pointer to an IDX3_fileinfo
 * struct, containing the read data.
 * @param filepath The path to the IDX3_ubyte file.
 * @return IDX3_fileinfo struct, containing the file headers and extra info.
 */
IDX3_fileinfo * readIDX3info(char * filepath);

/***
 * This function takes in the file path, reads the header bytes
 * of the IDX1_ubyte file and returns a pointer to an IDX1_fileinfo
 * struct, containing the read data.
 * @param filepath The path to the IDX1_ubyte file.
 * @return IDX1_fileinfo struct, containing the file headers and extra info.
 */
IDX1_fileinfo * readIDX1info(char * filepath);

/***
 * This function takes a read IDX3_ubyte file header and puts the
 * image contents of the file in a contiguous block of memory,
 * which is the size of a MNIST image times the number of images in the file.
 * @param fileinfo A pointer to an IDX3_fileinfo struct, containing the file info.
 * @return a pointer to the block of memory containing the images.
 */
MNIST_img * readIDX3content(IDX3_fileinfo * fileinfo);

/***
 * This function takes a read IDX1_ubyte file header and puts the
 * label contents of the file in a contiguous block of memory,
 * which is the size of a MNIST label times the number of labels in the file.
 * @param fileinfo A pointer to an IDX1_fileinfo struct, containing the file info.
 * @return a pointer to the block of memory containing the images.
 */
MNIST_lbl * readIDX1content(IDX1_fileinfo * fileinfo);

#endif //C_MNIST_READ_MNISTFILE_HS
