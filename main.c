/*
 * File: main.c
 * Author: Boyan Atanasov
 * License: Lincesed under the MIT License (see LICENSE.MD), 2017
 * Project: c_mnist_read
 */

#include "mnist_file.h"

int main() {

    IDX3_fileinfo * idx3info = readIDX3info("/home/boyan/Files/programming_c/c_mnist_read/mnist_data/t10k-images.idx3-ubyte");
    IDX1_fileinfo * idx1info = readIDX1info("/home/boyan/Files/programming_c/c_mnist_read/mnist_data/t10k-labels.idx1-ubyte");

    MNIST_img * arr_images = readIDX3content(idx3info);
    MNIST_lbl * arr_labels = readIDX1content(idx1info);

    for (int i = 0; i < 28; i++) {
         for (int k = 0; k < 28; k++) {
             if ((arr_images + 1)->img_pixel[i][k] > 0) {
                 printf("M");
             } else {
                 printf("_");
             }
         }
        printf("\n");
    }

    free(idx3info);
    free(idx1info);

    return 0;
}